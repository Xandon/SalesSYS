package br.com.senac.salesys0;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class resultado extends AppCompatActivity {

    private List<String> uf = new ArrayList<>();
    private cliente cliente;
    public static final String CLIENTE = "cliente" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);




        uf.add("AC");
        uf.add("AL");
        uf.add("AP");
        uf.add("AM");
        uf.add("BA");
        uf.add("CE");
        uf.add("DF");
        uf.add("ES");
        uf.add("GO");
        uf.add("MA");
        uf.add("MT");
        uf.add("MS");
        uf.add("MG");
        uf.add("PA");
        uf.add("PB");
        uf.add("PR");
        uf.add("PE");
        uf.add("PI");
        uf.add("RJ");
        uf.add("RN");
        uf.add("RS");
        uf.add("RO");
        uf.add("RR");
        uf.add("SC");
        uf.add("SP");
        uf.add("SE");


        Spinner spinner = findViewById(R.id.Estado);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, uf);

        spinner.setAdapter(adapter);



        final EditText editavelNomeCompleto = findViewById(R.id.EditavelNomeCompleto);
        final Spinner editavelCidade = findViewById(R.id.Cidade);
        final Spinner editavelUf = findViewById(R.id.Estado);
        final EditText editavelProfissao = findViewById(R.id.EditavelProfissao);
        final EditText editavelEmpresa = findViewById(R.id.EditavelEmpresa);
        final EditText editavelTel = findViewById(R.id.EditavelTelefone);
        final EditText editavelEmail = findViewById(R.id.EditavelEmail);
        final EditText editavelObs = findViewById(R.id.EditavelObs);
        final Button btnSalvar = findViewById(R.id.btnSalvar);
        final Button btnCancelar = findViewById(R.id.btCancelar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String nome = editavelNomeCompleto.getText().toString();
                String cidade = (String) editavelCidade.getSelectedItem();
                String uf = (String) editavelUf.getSelectedItem();
                String profissao = editavelProfissao.getText().toString();
                String empresa = editavelEmpresa.getText().toString();
                String tel = editavelTel.getText().toString();
                String email = editavelEmail.getText().toString();
                String obs = editavelObs.getText().toString();

                cliente cliente = new cliente(nome,cidade,uf,profissao,empresa,tel,email,obs);


                Intent intent = new Intent();
                intent.putExtra(CLIENTE , cliente);

                setResult(RESULT_OK,intent);

                Toast.makeText(resultado.this,"Salvo com sucesso!",Toast.LENGTH_LONG).show();
                finish();

            }
        });


        Intent intent = getIntent() ;

        cliente = (cliente) intent.getSerializableExtra(MainActivity.CLIENTE) ;


    }


}
